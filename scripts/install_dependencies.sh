#!/bin/bash

apt-get update &&
apt-get install -y software-properties-common git &&
apt-get install -y graphviz && # for diagramming in code documentation

add-apt-repository -y ppa:jonathonf/python-3.6 &&
apt-get update &&
apt-get install -y python3.6-dev &&
apt-get install -y build-essential python3-pip &&

# Ubuntu uses Python 3.5. To avoid confusion, we have to use `python3.6 -m pip`
# in place of `pip3` or `pip`. 
python3.6 -m pip install --upgrade pip &&
python3.6 -m pip install virtualenv tox &&

update-alternatives --install /usr/bin/python python /usr/bin/python3 1 &&

# for Shop2 planner
# apt-get install -y sbcl
# apt-get install -y cl-quicklisp
# sbcl --load /usr/share/cl-quicklisp/quicklisp.lisp \
#      --eval '(quicklisp-quickstart:install)'       \
#      --eval '(ql:add-to-init-file)'                \
#      --eval '(quit)'

# for planner
apt-get install -y cmake g++ g++-multilib &&
cd `pwd`/libs/thirdparty/fast_downward_planner && ./build.py && cd - &&

cd `pwd`/libs/thirdparty/VAL && make clean && make && cd - &&

# cd `pwd`/libs/thirdparty/pythonpddl && python setup.py build && cd -

