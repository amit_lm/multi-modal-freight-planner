#!/bin/bash
#
# Usage: `source <path to this script>.sh`
#
# This activates the virtual environment, like:
# ```
# (.venv) $
# ```
# To exit the virtual environment, use `deactivate`:
# ```
# (.venv) $ deactivate
# ```

source .venv/bin/activate

# For all
export COMPANY_LOWERCASE=manvik
export APP_NAME=manvik-colab-planner
export APP_DISPLAY_SHORT_NAME="Colab Planner"
export APP_DISPLAY_LONG_NAME="Colab Planner for Multi-Modal Freight Transport"

#   documentation via PlantUML
export PLANTUML_JAR=`pwd`/libs/thirdparty/plantuml/plantuml.jar

# For ml_common
export _ML_COMMON_LIB_DIR=`pwd`/libs/manvik_labs_common

# For planner
export _VAL_DIR=`pwd`/libs/thirdparty/VAL

# For app
export DATABASE_URL="sqlite:////var/tmp/colab_planner.db"
export FLASK_APP=mmf_planner/app/main.py
export FLASK_ENV=development

# Putting together
export PYTHONPATH=$PYTHONPATH:$_ML_COMMON_LIB_DIR
export PATH=$PATH:${_VAL_DIR}

# To be provided at runtime TODO
export APP_SETTINGS=config.DevelopmentConfig
