ack -f ps tests docs scripts conf \
    manage.py requirements.txt tox.ini \
    --ignore-dir=tests/files --print0 \
    | xargs -0 sloccount --addlang sql
