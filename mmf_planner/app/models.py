from app_init import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Domain(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(128), unique=True)
    text = db.Column(db.Text())
    name = db.Column(db.String(512), index=True, unique=True)

    def __repr__(self):
        return '<Domain {}>'.format(self.name)


class Plan(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(db.String(128), unique=True)
    domain_id = db.Column(db.Integer, db.ForeignKey('domain.id'), unique=True)
    text = db.Column(db.Text())
    name = db.Column(db.String(512), index=True, unique=True)

    def __repr__(self):
        return '<Plan {}>'.format(self.name)
