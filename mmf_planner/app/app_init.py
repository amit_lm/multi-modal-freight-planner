import os
import flask
from flask_sqlalchemy import SQLAlchemy
from planner import PlanSearcher
from ml_common.utils.logging import initialize_logging


class MyFlask(flask.Flask):
    def __init__(self, name, **kwargs):
        initialize_logging()
        self.planner = PlanSearcher()
        super().__init__(name, **kwargs)
        self.config.from_object(os.environ['APP_SETTINGS'])
        self.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


app = MyFlask("colab_planner")
app.template_folder = os.path.join(app.config['APP_DIR'], "templates")
app.static_folder = os.path.join(app.config['APP_DIR'], "static")
db = SQLAlchemy(app)
