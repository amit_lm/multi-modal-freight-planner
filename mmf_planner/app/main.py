import os
import flask
from flask_classful import FlaskView
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView as AdminModelView
import decorators
import forms
from app_init import app, db
from models import User, Domain, Plan  # noqa


class PlanView(FlaskView):
    def index(self):
        return "Hello World!"

    def get(self, id):
        return "Hello {}!".format(id)


class DomainView(FlaskView):
    def index(self):
        return "Hello World!"

    def get(self, id):
        return "Hello {}!".format(id)


class UserView(FlaskView):
    def index(self):
        return "Hello World!"

    def get(self, id):
        return "Hello {}!".format(id)


PlanView.register(app, route_prefix='/plans/')
DomainView.register(app, route_prefix='/domains/')
UserView.register(app, route_prefix='/users/')


@app.route('/')
@decorators.templated()
def index():
    pass


@app.route('/login', methods=['POST', 'GET'])
@decorators.templated()
def login():
    form = forms.LoginForm()
    if form.validate_on_submit():
        flask.session['username'] = flask.request.form['username']
        flask.flash(
            'Welcome, %s' % flask.escape(flask.session['username']))
        return flask.redirect(flask.url_for('index'))

    # the code below is executed if the request method
    # was GET or the credentials were invalid
    return dict(form=form)


@app.route('/logout')
def logout():
    # remove the username from the session if it's there
    flask.session.pop('username', None)
    flask.flash('You were logged out')
    return flask.redirect(flask.url_for('index'))


@app.errorhandler(404)
def not_found(error):
    return flask.render_template('not_found.html'), 404


@app.route('/favicon.ico')
def favicon():
    return flask.send_from_directory(
        os.path.join(app.root_path, 'static'),
        'favicon.ico',
        mimetype='image/vnd.microsoft.icon')


class AdminUserView(AdminModelView):
        can_delete = False  # disable model deletion


class AdminPlanView(AdminModelView):
        page_size = 50  # the number of entries to display on the list view


class AdminDomainView(AdminModelView):
        page_size = 50  # the number of entries to display on the list view


admin = Admin(app, name='Colab Planner Admin', template_mode='bootstrap3')
admin.add_view(AdminUserView(User, db.session))
admin.add_view(AdminPlanView(Plan, db.session))
admin.add_view(AdminDomainView(Domain, db.session))


if __name__ == '__main__':
    host = '0.0.0.0'  # ] + ([app.config.get('PUBLIC_URL', None)] or [])
    port = app.config.get('HOST_PORT', None) or 5000
    app.run(host=host, port=port, debug=True)
