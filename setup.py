#!/usr/bin/env python

from setuptools import find_packages
from setuptools import setup


# Example of setup file:
#   https://github.com/nylas/sync-engine/blob/master/setup.py
setup(name='Multi-modal freight planner',
      version='1.0',
      description='Engine',
      author='Amit Kumar, Vaibhav Vishnoi',
      maintainer='Amit Kumar, Vaibhav Vishnoi',
      author_email='in4tunio@gmail.com, vvishnoi@yahoo.com',
      maintainer_email='in4tunio@gmail.com, vvishnoi@yahoo.com',
      url='',
      packages=find_packages(),
      scripts=[],
      data_files=[],
      install_requires=[
          # Basic requirements
          'dataclasses>=0.6,<0.7',
          'dataclasses-json<0.1',
          'delegator.py>=0.1.0,<0.2.0',  # "subprocess for Humans"

          # Requirements for manager
          'click>=6.7',

          # Requirements for core
          'airflow>=1.8.0,<1.9.0',
          'dask>=0.18.0,<0.19.0',

          # Requirements for GUI
          'bokeh>=0.13.0,<0.14.0',
          'holoviews>=1.10.0,<1.11.0',
          'networkx>=2.1,<2.2',
          'jupyter>=1.0.0,<1.1.0',

          # Requirements for app
          # jinja2<2.9.0,>=2.7.3 is required by {'airflow'}
          'jinja2>=2.7.3,<2.9.0',
          # not using flask 1 due to airflow dependency on older jinja2 version
          'flask>=0.11.0,<0.12',
          'Flask-Classful',
          'Flask-Migrate>=2.2.0,<2.3.0',
          'Flask-Security',
          'speaklater',
          'blinker',
          'Flask-Script',
          'Flask-Admin',
          'Mako',
          'python-editor',

          # Requirements for tox
          'tox>=3.0.0,<3.1.0',
          'tox-pip-extensions>=1.2.0,<1.3.0',
          'pytest>=3.7.0,<3.8.0',
          'pylama>=7.4.0,<7.5.0',
          'bdd>=0.2,<0.3',

          # Requirements for documentation
          'sphinx>=1.7.0,<1.8.0',
          'recommonmark>=0.4.0,<0.5.0',
          'sphinxcontrib-plantuml>=0.12',
          # sphinxcontrib-seqdiag==0.8.1

          # Requirements for deployment
          'honcho>=1.0.0,<1.1.0',
      ])
