import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'Iehie9ieaiF9DeeSEiw1aeso'
    SQLALCHEMY_DATABASE_URI = os.environ['DATABASE_URL']
    APP_DIR = os.path.join(basedir, "mmf_planner/app")


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    PUBLIC_HOST_IP = '13.67.91.144'
    HOST_PORT = 80


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
