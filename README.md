# Multi-modal Freight handling and Transportation

## Development Setup

We have tested the setup below on:

* Linux Mint 18.1 Serena (Ubuntu 16.04).

#### Linux

Do the following for one time set up:

1. Install base dependencies: Install Python 3.6+ developer libraries and other
   dependencies as listed at `scripts/install_dependencies.sh`. May require
   `sudo` privileges.
2. We recommend the use of `virtualenv`. Run the command
   `sh scripts/init_virtualenv.sh` to set up the `virtualenv`
   with its files under the `.venv` directory.

During development, follow these steps:

1. Activate `virtualenv` (name `.venv`) by running
   `source scripts/virtualenv.sh`. Use `deactivate` to exit the virtual
   environment at any time. This script also sets PYTHONPATH and other
   environment variables.
2. For first-time use as well as whenever you update the requirements of
   the project (in `requirements.txt` or `setup.py`), be sure to run
   `python setup.py develop` to update-install the requirements.

To check the setup, you can run tests (see section `Running tests` below).

### Running tests

Use `tox -e tests` for running tests via `pytest`. To select tests to run
based on their name, use the `-k` option of `pytest`, e.g.,
`tox -e tests -- -k "test_elastic"`. Here the `--` option is used to pass
all following parameters to `pytest`.

The same command can be used within `libs/manvik_labs_common` directory to
test the `ml_common` package.

### More Commands

The following commands are useful during development:

* `tox`: to build documentation (`tox -e docs`), for testing doctests,
    coverage, flake8 and so on. Please check Tox documentation and
    `tox.ini` to know the full list of commands.
* `sh scripts/,,,.sh`: for generating ctags, for sloccount, etc.
* Use `honcho start` to start web app, Elasticsearch, and other required
    services (as specified in `Procfile`).
* For database migrations, we use Flask-Migrate. To know more, use 
  `python mmf_planner/app/models_manage.py --help`. 

<!-- 
## Using Gitlab CI/CD

Gitlab CI reads the settings within `.gitlab-ci.yml` to run your jobs.
Gitlab.com provides 2000 mins per month of computation in the free version.

Here are the steps to verify your `.gitlab-ci.yml` file on your local machine:

1. Install [Gitlab Runner](https://docs.gitlab.com/runner/install/).
2. Inside your project root directory (that contains `.gitlab-ci.yml`), use
   Gitlab Runner on your local computer, e.g.,
   `gitlab-runner exec docker build_` if you have want to verify the
   stage named `build_`.

## Release History

TODO
   -->
