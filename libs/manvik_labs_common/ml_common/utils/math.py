from typing import List, Iterable


def argsort(seq: List, reverse: bool=False) -> List:
    """ Return:
            An array of indices in sorted order of values in `arr`.

        >>> a = ['3', '2', '1']
        >>> s = argsort(a)
        >>> s
        [2, 1, 0]
        >>> [a[i] for i in s]
        ['1', '2', '3']
    """
    return sorted(range(len(seq)), key=seq.__getitem__, reverse=reverse)


def unzip(iterable: Iterable):
    """ Unzip a zipped iterable.

        >>> a = [1, 2, 3]
        >>> b = [2, 4, 6]
        >>> c, d = unzip(zip(a, b))
        >>> c
        (1, 2, 3)
        >>> d
        (2, 4, 6)
    """
    return zip(*iterable)
