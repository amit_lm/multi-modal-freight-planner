import logging
import sys


def initialize_logging():
    """ Set up format, etc., for logging via all modules """
    # TODO: For GAE, use https://stackoverflow.com/a/28743317/19501

    format2 = '%(levelname)s %(thread)d %(filename)s:%(lineno)d- %(message)s'
    MIN_LEVEL = logging.DEBUG

    logging.captureWarnings(True)

    stdout_handler = logging.StreamHandler(sys.stdout)
    stdout_handler.setFormatter(logging.Formatter(format2))
    stdout_handler.setLevel(MIN_LEVEL)
    logging.getLogger().addHandler(stdout_handler)

    logging.getLogger().setLevel(MIN_LEVEL)


def snip_debug(inp, fn=str, length=100, endlen=10, sep='...'):
    """
    This function trims the input string to the provided maximum length.
    `fn` is a function like `str` or `repr` to convert `inp` to a string.
    The algorithm runs as follows.
    First, convert `inp` into its string representation `txt` by calling
    `fn(txt)`.
    Snip `txt` to at most `length` characters.
    Show the first part of `txt`
    and the last `endlen` characters of `txt`.
    Use `sep` as the separator of first and last part.

    >>> snip_debug('abcdefghijkl', length=8, endlen=2)
    'abc...kl'
    """
    txt = fn(inp)
    ll = length - len(sep) - endlen
    return txt[:ll] + sep + txt[-endlen:] if len(txt) > length else txt
