import sys


def is_kernel():
    """Return whether we can use interactive elements of IPython

    This function is particularly useful to write pure-Python version
    of IPython Magic commands.
    """
    if 'IPython' not in sys.modules:
        # IPython hasn't been imported, definitely not
        return False
    from IPython import get_ipython
    # check for `kernel` attribute on the IPython instance
    return getattr(get_ipython(), 'kernel', None) is not None


def get_ipython_2():
    """Same as `get_ipython()`, but with error check

    Useful for running pure-Python version of IPython Magic commands via
    the `run_line_magic()` and `run_cell_magic()` members.
    """
    if is_kernel():
        from IPython import get_ipython
        return get_ipython()
    else:
        raise RuntimeError("IPython or kernel not found. Are these installed?")
