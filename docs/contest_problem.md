# Multi-modal Freight handling and Transportation

<https://www.movehack.gov.in/campaign/1/about>

## Problem Statement

Design and build a common platform which helps in increasing the efficiency of ports and freight train terminals for loading, unloading and
transportation of goods in order to increase handling capacity, reduce dwell time, increase throughput and provide multi-modal transport
options in an efficient manner.

## Context

The Turn-Around Time (TAT) for major ports in India has progressively decreased from 117 hours in 2011-12 to 64 hours in 2017-18. However the Singapore port has a TAT that is less than 24 hours, which is where we aspire to be. Under the 'Sagarmala Programme' of Government of India, Rs. 8.5 trillion investment is envisaged for enhancement of port connectivity via road, rail, multi-modal logistics parks, pipelines & waterways and promote coastal community development, which would result in boosting merchandise exports by US $110 billion, and generation of around 10,000,000 direct and indirect jobs. These logistics bottlenecks cause substantial economic loss impacting manufacturers, traders and consumers.

## Data

Jawaharlal Nehru Port Trust (JNPT) Data Set for freight handling contains details of containers handled by the port (both import and export). The zip file contains 6 months' data from Jan-June.

Freight movement by Indian Railways for container movement by Railways to the port JNPT for the months of January-June, 2018.

This report by DMICDC Logistics Data Services Limited on the following link provides detailed benchmark information about Dwell Time of Container Movement around JNPT region.

## Submission Information and Context

The top 30 teams from the online submissions round will travel to Singapore from 1st to 2nd September, 2018, and will be mentored by a curated group of top experts, advising the teams on a variety of parameters including design improvement, business viability, technical solution and customer targeting/marketing.

The top 20 teams from Singapore round will participate in the final round which will be held in New Delhi, India on 5th and 6th September, 2018. Prizes will be announced during the MOVE summit to be held on 7th and 8th September, 2018.

...

The idea/solutions will be scored by the Jury taking into account categories, including but not limited to, the following categories:

Innovation | Technology | Market Fit | Viability | Scalability

## Timeline

By 11:59 PM IST (Indian Standard Time), 23rd August 2018:

* Write-up of 200 words to describe the overview of the solution
* Brief presentation (up to 5 slides) on the idea submitted

By 11:59 PM IST (Indian Standard Time), 28th August 2018:

* Presentation on the architecture of the solution (up to 10 slides)
* Solution code repository (Github links, executable files)
* Detailed demo video
* Implementation testimonials, if any

## Other resources

Here are a few references from outside the Movehack website.

### Submitted by Slack members / mentors

* [Data contributed by Ministry of Shipping](https://data.gov.in/ministrydepartment/ministry-shipping)
* [Digital and technology: Port specific gaps across modules](https://slack-files.com/TC209PM5Z-FC8QARL66-c31218423b) (recommended)
* [Projected road freight and passenger traffic for 12th five-year plan](https://data.gov.in/catalog/projected-road-freight-and-passenger-traffic-twelfth-five-year-plan)

### Submitted by Amit

#### Videos

* DMICDC Logistics Data Services Limited (video, recommended, Aug 2017):
  <https://www.youtube.com/watch?v=vEn-sc59dw0>
* Panel discussion about the DPD model (video, recommended, March 2017):
  <https://www.youtube.com/watch?v=Zq52ULsORbk>
* How Direct Port Delivery is transforming logistics sector
  (video, April 2018):
  <https://www.youtube.com/watch?v=lO4eEOcIO5M>
* Gate to Gate: What Happens When a Truck Picks Up a Container? (video, April 2014) <https://www.youtube.com/watch?v=P9IJN1yIIJ4>