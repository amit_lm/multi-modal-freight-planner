#!/bin/bash
set +x
dir=.
pandoc -t beamer $dir/presentation.md -o $dir/presentation.pdf --latex-engine=/Library/TeX/texbin/pdflatex
