#!/bin/bash
set +x

sudo apt-get install pandoc texlive-latex-base latex-beamer 
sudo apt-get install pgf lmodern texlive-fonts-recommended
sudo apt-get install pgf lmodern texlive-fonts-extra 

