# README

This submission should contain the following documents:

* Presentation on the architecture of the solution (up to 10 slides)
* Solution code repository (Github links, executable files)
* Detailed demo video
* Implementation testimonials, if any

## Build

Run `install_dependencies.sh` to install the dependencies.
Use `make` to build `presentation.pdf`. 
