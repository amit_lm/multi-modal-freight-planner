---
author: Amit Kumar, Vaibhav Vishnoi
title: "Solution Architecture: Collaborative planning platform for multi-modal freight transportation"
date: Aug 28, 2018
---

# Background: Planning & Execution (1)

* \scriptsize Planning is the act of coming up with a set of actions to achieve pre-decided 
  goals. While playing a solitaire card game like Freecell, the player performs
  planning. AI Planning can be used to support autonomous activities in several
  domains such as in Mars rovers, transportation, satellites, and robotics.
  An automated planner, when provided a model of 1) __actions__ (e.g., `drive`) and 2) __objects__ (e.g., `truck`s, map of routes) in a
  particular __domain__, 3) the __initial/current state__ (initial location of trucks) and 4) certain desired optimization or satisfaction __goals__(e.g., goal location of trucks), computes 
  and returns a set of actions that can be used to reach the goals from 
  the initial state.[^RusselNorvig10] \normalsize
* \scriptsize Every action has __conditions__ (e.g., in a logistics domain, a `drive` action 
  requires a truck is available), consumes some __resources__ (driving consumes fuel, engages driver) and makes __contributions__ to the 
  state of objects (location of the truck). These are defined in the input action model. 

[^RusselNorvig10]: \tiny Russell, Norvig & Davis (2010). Artificial intelligence: a
modern approach. 3rd ed. Upper Saddle River, NJ: Prentice Hall. \normalsize


# Background: Planning & Execution (2)

* \scriptsize In a dynamic and uncertain world, contingencies are common.
    Truck went for repair and is unavailable, for example. Or, export cargo is held
    up in customs.
* \scriptsize Before plan can be executed, plan __validation__ 
    ensures that all the actions in the plan are still valid, that is, the 
    pre-conditions of all the actions in the plan are satisfied.
* \scriptsize Plan __execution__ invokes __actuators__ to make changes to the
    world. __Sensors__ provide the information about the updated state of
    the world. Based on the updated state, __replanning__ may be called.
* \scriptsize As a part of planning, the planner may carry out shallow or deep 
    __simulations__, that is, what-if scenario analysis. 

# Recap: Collaborative planning platform

\tiny Refer: Presentation in the first submission \normalsize

## Goal

* Key idea: A new collaborative platform that planning managers across port departments can 
    use to: 
    * formulate plans at all levels of decision-making, 
    * invoke automated planning software,
    * share and discuss about plans in real time with other managers,
    * allocate shared port resources, and
    * execute plans.
* Dashboard, UI/UX to visualize and edit plans.
* Integrate with existing software such as TOS/POS, 
  optimization, simulation, scheduling software.

## Customers / Metrics

* Our customers are planning managers across port departments.
* Metrics: Usage / adoption; business / operational.

# The Planning Process Flow

![Planning Process Flow](files/planning_process_flow.png){ width=100% } \


# Prototype: Implementation

## Features implemented

Planning manager can:

* manually make plans,
* store plans,
* invoke the automated planner by providing inputs of
  a domain model, an initial state and goals,
* validate plans, and
* invoke plan execution.

## Features not yet implemented

* share plans with other managers
* collaboratively edit plans

# Demo of Use Cases 

Currently, the web application development is a work-in-progress and not in a
presentable format. Towards completeness for this milestone, we made 
a simpler console application. The source code repository link is provided in
the next slides.


![Demo of use case](files/demo.png){ width=70% } \


# System architecture

<!--

This slide covers: system architecture, implementation details

-->

<!--

I have used PlantUML via https://www.planttext.com/ to 
draw diagrams used in the presentation. I saved the PlantUML input to
`files/system_architecture_diagram.uml`.

-->

![System architecture](files/system_architecture_diagram.png){ width=100% } \

<!--
Web app: Flask
User Mangement - Flask-Security
Chat: Slack/Flask
Airflow
Dask
Planner (JAPE) - ANML text representation of the plan
Viz (Bokeh)

Framework

* Planning: Resources Consumed/Demanded, Risks, Visualization, Sharing plans, Diff of plan versions, store history of plans
* Backward compatibility 
* Simulation
* Integration with optimizers
* Sensory inputs
* Notifications
* Activity Logs
-->

# Discussion and Future Work

## Implementation Details

* Source code: <https://gitlab.com/amit_lm/multi-modal-freight-planner>
* Thirdparty libraries used: 
    * Planner: [Fast Downward](http://www.fast-downward.org/ObtainingAndRunningFastDownward)
    * Plan Validator: [VAL](https://github.com/KCL-Planning/VAL)
    * (not yet integrated) Plan Execution Engine: [Airflow](https://airflow.apache.org/)

## Contributions

* Created a plan database to store planns
* Created a platform to invoke planning system, plan validator and plan executor.

## Discussion

* Usage of open technologies and the powerful Python ecosystem will foster innovation.

## Future Work

* Integrated the open-source Airflow workflow engine for plan execution.
* Build and integrate with more expressive planning system, that is, one that works with more
    expressive domain model (e.g., numerical contributions, complex actions
    with multiple parts, notion of current time, etc). 


