.. Multi Modal Freight Planner documentation master file, created by
   sphinx-quickstart on Wed Aug 15 20:44:46 2018.

.. _contents:

Multi Modal Freight Planner
===========================
.. toctree::
   :maxdepth: 2

   introduction
   editing

Architecture Diagram
--------------------

.. uml::

      @startuml

      'style options 
      skinparam monochrome true
      skinparam componentStyle uml2
   
      component [Website, App] as UI
      component [Application] as Application {
          component Datastore 
          component SearchEngine
      } 
      component [Data Enricher] as DataEnricher
      component Crawler
   
      interface "Property Search API\n<<interface>>" as SearchAPI
      interface "Property Ingestion API\n<<interface>>" as IngestionAPI
   
      actor Buyer
   
      Buyer - UI
      UI -( SearchAPI
      SearchAPI - Application 
      IngestionAPI -up- Application 
      DataEnricher -( IngestionAPI
      Crawler - DataEnricher

      @enduml

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
