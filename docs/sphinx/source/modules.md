```eval_rst
:orphan:
```

# Modules

## Planner Model

### `Item` and related classes

```eval_rst
.. module:: mmf_planner.core.main
.. autoclass:: Item
   :members:
   :undoc-members:
```