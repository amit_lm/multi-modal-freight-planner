# How to edit this documentation

We prefer Markdown over RST for Sphinx documentation, and for this purpose we use Recommonmark (see section below on Recommonmark).

Also, we prefer Google Style Docstrings over RST docstrings. For this purpose, we use the Napoleon extension (see section below on Napoleon).

## Recommonmark

Whereas RST is the default syntax for Python documentation using Sphinx, Markdown is more common, and thus people across departments of our company can use Markdown for collaboration. For this reason, we prefer the Markdown syntax over RST for Python documentation.

Sphinx officially supports [Recommonmark](http://www.sphinx-doc.org/en/stable/markdown.html?highlight=markdown) as the way to use Markdown for documentation. This post is about a couple of topics that are not straightforward while using Recommonmark.

### Using autodoc to generate documentation from docstings

Developers use Sphinx with [autodoc](http://www.sphinx-doc.org/en/master/ext/autodoc.html#module-sphinx.ext.autodoc) to generate documentation from docstrings within Python modules. It is possible to generate the documentation fully automatically using [apidoc](http://www.sphinx-doc.org/en/master/man/sphinx-apidoc.html). Altneratively, using Recommonmark, developers can use autodoc to generate more customized documentation as follows.

Suppose we have a project consisting of modules, one of which is the module `com.example.physics` having a class `PhyObject` and a function `kinetic_energy` (among others):

```
class PhyObject:
    """Represents a physical object. """
    def __init__(self, mass):
       """
       Attributes:
           mass (float): mass of the object in grams.
       """
       self.mass = mass


def kinetic_energy(mass, vel):
    """Compute kinetic energy.

       A more detailed description of the function.
       Args:
           mass (float): mass of the object in grams.
           vel (float): velocity of the object in meters per second.
    """
    return 0.5*mass*vel*vel
```

Here we have used [Google-style docstrings](http://www.sphinx-doc.org/en/stable/ext/napoleon.html).

To generate the documentation for the modules in the project using autodoc, we write a file `modules.md` which contains a section for each of the modules:

````
# Modules
...
## Physics
...
### `PhyObject` and related classes
```eval_rst
.. module:: com.example.physics
.. autoclass:: PhyObject
.. autoclass:: ImagObject
.. autofunction:: kinetic_energy
```
...
## Other Modules
...

````
Note here that `eval_rst` is the syntax within [Recommonmark Autostructify component](http://recommonmark.readthedocs.io/en/latest/auto_structify.html) that allows us to embed RST directives within Markdown. Usage of `eval_rst` requires you to enable Autostructify first (refer the Recommonmark documentation).

That's it. Upon running Sphinx-build, we can see `modules.html` containing autodoc-generated documentation.

### Referencing Python Modules

Developers often want to refer to Python modules from within Sphinx documentation. Here is how to do it using Recommonmark.

First, we need to set up Autostructify's `url_resolver` so that `modules::path.to.class` resolves to the class documentation within `modules.html`, that is, `modules.html#path.to.class`. For this set up, we need to add the following to `conf.py`:
```
import re

def my_url_resolve(url):
    regex = 'modules::(.*)'
    m = re.match(regex, url)
    if m:
        module = m.group(1)
        return "modules.html#%s" % module

app.add_config_value('recommonmark_config',
                     {'url_resolver': my_url_resolve},
                     True)      
```
After this, within our documentation `physics.md`, we can write:
```
The output of this function is a list of [PhyObject](modules::com.example.physics.PhyObject) objects.
```
which will appear in `physics.html` after Sphinx-build like:

> The output of this function is a list of [PhyObject](modules::com.example.physics.PhyObject) objects.

## Sphinx-Napoleon

[Sphinx-Napoleon](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/) is useful to generate Sphinx documentation from Google-style docstrings.

[Here is an example](http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) of Google-style docstrings.
