# The multi-modal freight planning problem

* trends in India
* industry trends: local and global trade, etc.


## Excerpts

### via [LimLee2015]

After 9/11, various security measures have been implemented in  maritime and port transportation. Carriers and port operators are improving their equipment and operation strategies in order to satisfy the regulations for environmental protection. The logistics market has changed from a supplier-oriented one to a customer-oriented one because the supply of logistics resources has exceeded the demand. Consequently, shipping liners have gained stronger negotiation power over port operators. In some cases, shipping liners demand a high performance level from terminals as part of the contract conditions, and this can include the throughput rate per berth or the turnaround time of a vessel or road trucks.


Optimizing operations plans of container terminals:

- Berth planning: schedules the usage of the quay by vessels.
- Loading/unloading sequencing
- Space planning, particularly on the yard. 

Possible improvements in the operations planning process:

- integrating planning activities. Traditionally operations plans are
    constructed in a hierarchical way. The plans in the higher
    hierarchies become the constraints to the plans in the lower hierarchies.
    Because of this hierarchical decision-making structure, some serious problems may
    arise in the lower hierarchy of a plan, which could be solved by a minor
    modification of a plan in an upper hierarchy.
- rescheduling capabilities should be sufficiently fast and should not disturb
    the on-going operations.
- automating the operations planning process can allow for service-level improvement. 
- information on shared resources should be made available among various
    planners.
- simulation techniques can be used to evaluate plans in advance
- collaborating activities with outside partners such as trucking companies,
    rail operators, vessel liners, etc., may include information sharing, 
    integrated scheduling, and devising economic measures for the collaboration


### via [BEARZOTTI201395]

Various disruptions are possible. Examples of internally disruptive events are:

* Berth unavailable because it must be repaired.
* Crane problems
* Truck problems
* Blocks unavailable
* Container problems

Some external events are:
* The need to discharge the vessel, replan the stowage and load the ship again.
* Delay of a ship.
* Truck problems.
* Damaged containers.
* The container does not meet the conditions (documentation or physical rquirements) to be imported/exported.
* Delays because of problems with the documentation submitted by the Customs Agency.
* Delays related to the care of the ship.
* The vessel does not meet legal standards for sailing.

Defines a hierarchical, multi-agent architecture for event management with one agent
responsible for handling disruptions within each part of the port.

### Via [JNPTEODB2017], [JNPTTECHGAPS2018]

Situation at JNPT: 

* DPD has reduced cost and import/export times drastically.
* TOS has been only partially adopted. Most often not used currently for 
  planning but only for recording data. Plans are usually formulated in 
  face-to-face meetings and recorded in Spreadsheets. 

## References

[LimLee2015] K H Lim and H Lee, Chapter 2 Container Terminal Operation: Current Trends and Future Challenges, 
C.-Y. Lee, Q. Meng (eds.), Handbook of Ocean Container Transport Logistics,
International Series in Operations Research & Management Science 220 

[pdf]

@article{BEARZOTTI201395,
title = "The Event Management Problem in a Container Terminal",
journal = "Journal of Applied Research and Technology",
volume = "11",
number = "1",
pages = "95 - 102",
year = "2013",
issn = "1665-6423",
doi = "https://doi.org/10.1016/S1665-6423(13)71518-9",
url = "http://www.sciencedirect.com/science/article/pii/S1665642313715189",
author = "Lorena Bearzotti and Rosa Gonzalez and Pablo Miranda",
keywords = "event management, container terminal, multi-agent system,
distributed control system"
}

[JNPTEODB2017] Presentation to World Bank on observation and
initiatives on 'Ease of Doing Business'

[pdf]

[JNPTTECHGAPS2018] Dr Abhijit Singh (Mentor, Movehack), Digital and Technology - Tech Gaps, Conversation on Movehack
Slack forum.

[pdf]

