# Solution

## Preface

Based on the criteria for evaluation specified on the contest website, the proposed solution should have the following characteristics:

- Innovation
  - Will require literature review of multi-modal freight transportation
  - Propose improvements
- Technology
  - AI Planning technology
    - Prediction of load via ML, and communication
    - Simulation of what-if scenarios
    - Need some kinds of beautiful charts
- Market Fit
  - Whether the solution is applicable, and how general it is
  - Use open technologies
- Viability
  - Solution should be minimally complete
- Scalability
  - Whether the solution applies to large dataset or large geographies

## Proposed Solution

### Main Ideas

1. The problem of planning
2. Overall idea: AI-assisted collaborative planning tool. Supports:
   * visualization of plan
   * collaborative, visual editing of plan
       * activity log
       * notifications
   * define the important constraints/actions in the plan, and leave the fine-grained details to the
       AI planner
   * support online sensory inputs and plan updates
   * scenario planning
   * web-based interface
   * integration with third-party optimizers
   * open source technologies for workflow management promotes innovation
   * support for scalable cluster environment


CONTROLS: EMULATION TO IMPROVE THE PERFORMANCE OF CONTAINER TERMINALS

Components:

- Modeling tool
- Planner input representation
- Planner core
  - Solver
- Planner output
- Sensing and execution via Airflow
- Planner GUI: VS Code extension or PhosphorJS with Mermaidjs

Modeling tools: Nodus Existing tools: STAN

AI automated planning has been successfully used

Features:

- Sensing
- Notification

Future work:

- Modeling and simulation of what-if scenarios

### Literature review: multimodal freight planning

https://www.ship-technology.com/news/newsindias-jnpt-selects-navis-system-to-upgrade-terminal-operating-system-at-jnpct-5949778/

### TODOs

Milestone 1 (Aug 20): Lit review complete, viz & planner feature complete
Milestone 2 (Aug 23): Example complete, first submission complete
Milestone 3 (Aug 28): Beautify documentation, demo video.

Milestone 1:

- Lit review
  - JNPT
  - Multi-modal freight planning
- Planner
  - Define and document input and output models, plan change objects, and json representation
  - sensory inputs
  - notifications
  - example domain
  - convert planning problem into constraint optimization problem
- Viz
  - Depict the json representation of the plan
  - Add interactions
- Future plans
  - Simulations
