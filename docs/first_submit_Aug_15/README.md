# README

This submission contains two documents:

* <overview.md>: Write-up of 200 words to describe the overview of the solution
* <presentation.md>: Brief presentation (up to 5 slides) on the idea submitted

## Build

Run `install_dependencies.sh` to install the dependencies.
Use `make` to build `presentation.pdf`. 
