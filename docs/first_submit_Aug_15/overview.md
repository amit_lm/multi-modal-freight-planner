# Collaborative planning platform for multi-modal freight transportation

Planning is crucial at all levels of decision-making---strategic, lasting over months, tactical, over weeks, and operational, over hours; and in all parts of the port---berth planning, yard planning, gates and so on. Collaborative planning capability can improve efficiency, reliability, flexibility, and sustainability at ports.

Towards such capability, we propose a web-based platform that planning managers across port departments can use to formulate plans at all levels of decision-making, invoke automated planning software, share and discuss about plans in real time, allocate shared port resources, and execute plans (via a workflow engine). The platform will offer UI/UX to visualize and edit plans, and will allow extensions with external software and integrations with existing software such as TOS/POS, optimization, simulation, scheduling software. Scalability will be facilitated by a compute cluster for use of the planning system.

However, due to the large size of the platform, which will take months to implement, the scope of the implementation within Movehack has to be restricted. The scope has been highlighted in the system architecture diagram within the attached presentation.

The metrics will be usage/adoption-related and business/operational.

Among our hypotheses, we believe that an easy-to-use collaborative planning tool can bring flywheel effects in the productive functioning of the port. To our knowledge, there are no such off-the-shelf collaborative planning tool. A hosted service with a user-friendly interface will make adoption easy.

The multimodal freight planning problem is quite open, which is a risk. Furthermore, the domain and the data are new for the authors and the prior art is not easily accessible.
