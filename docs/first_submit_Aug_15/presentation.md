---
author: Amit Kumar, Vaibhav Vishnoi
title: Collaborative planning platform for multi-modal freight transportation
date: Aug 25, 2018
---

# The opportunity 

<!--

This slide covers: opportunity, supporting context

-->

* Indian ports will experience a massive capacity increase up to 3500 MTPA by 2025 from the present 1500 MTPA.[^sagarmala] 
* At JNPT, Direct Port Delivery (DPD), infrastructure improvements and 
  modern Terminal/Port Operating System (TOS/POS) are important ongoing 
  initiatives [^DPDEODB2017][^JNPTMasterPlan2016][^JNPTTOS]. 
* __Planning__ is crucial at all levels of 
    decision-making---_strategic_ lasting over months, 
    _tactical_ over weeks and _operational_ over hours; and in all parts of 
    the port---berth planning, yard planning, gates and so on.
* Collaborative/integrated planning capability
can improve efficiency, reliability, flexibility, and 
sustainability at ports.[^MultimodalLitReview] 

[^sagarmala]: \tiny Port Modernization http://sagarmala.gov.in/project/port-modernization-new-port-development \normalsize
[^DPDEODB2017]: \tiny Ease of business at JNPT  http://dipp.nic.in/sites/default/files/TAB_EoDB_26April2017.pdf \normalsize
[^JNPTTOS]: \tiny JNPT selects N4 TOS; PIB Press Release (2017) http://pib.nic.in/newsite/PrintRelease.aspx?relid=171678 \normalsize
[^JNPTMasterPlan2016]: \tiny JNPT Master Plan (2016) http://sagarmala.gov.in/sites/default/files/4.Final%20Master%20Plan_JNPT.pdf \normalsize
[^MultimodalLitReview]: \tiny e.g., see SteadieSeifi et al (2013): Multimodal freight transportation planning: A literature review \normalsize

# Collaborative planning platform

<!--

This slide covers: problem, goals, customers, metrics

-->
## Goal

* Key idea: A new collaborative platform that planning managers across port departments can 
    use to: 
    * formulate plans at all levels of decision-making, 
    * invoke automated planning software,
    * share and discuss about plans in real time with other managers,
    * allocate shared port resources, and
    * execute plans.
* Dashboard, UI/UX to visualize and edit plans.
* Integrate with existing software such as TOS/POS, 
  optimization, simulation, scheduling software.

## Customers / Metrics

* Our customers are planning managers across port departments.
* Metrics: Usage / adoption; business / operational.

# System architecture

<!--

This slide covers: system architecture, implementation details

-->

<!--

I have used PlantUML via https://www.planttext.com/ to 
draw diagrams used in the presentation. I saved the PlantUML input to
`files/system_architecture_diagram.uml`.

-->

![System architecture](files/system_architecture_diagram.png){ width=100% } \

<!--
Web app: Flask
User Mangement - Flask-Security
Chat: Slack/Flask
Airflow
Dask
Planner (JAPE) - ANML text representation of the plan
Mesa (advanced feature) 
Viz (Bokeh)

Framework

* Planning: Resources Consumed/Demanded, Risks, Visualization, Sharing plans, Diff of plan versions, store history of plans
* Backward compatibility 
* Simulation
* Integration with optimizers
* Sensory inputs
* Notifications
* Activity Logs
-->

# Hypotheses and Risks

<!--

This slide covers: UX, wireframes, hypotheses, risks

-->

## Hypotheses

* An easy-to-use collaborative planning tool can bring flywheel effects in the
    productive functioning of the port.
* There are no such off-the-shelf collaborative planning software.
* A hosted service with a user-friendly interface will make adoption easy.
* Productivity should be facilitated via minimizing mandatory inputs to the 
  planning system from the user.

## Risks

* The multimodal freight planning problem is quite open. 
* The domain and the data are new for the authors.
* Prior art is not easy accessible.


